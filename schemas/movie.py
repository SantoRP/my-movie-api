
from pydantic import BaseModel
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional


class Movie(BaseModel): ###esquema de datos con la libreria pydantic. Aqui agregamos todos los atributos de la pelicula 
    id: Optional[int] = None ##de libreria  typing, optional indica que el valor puede ser opcional
    title: str = Field(min_length=5, max_length=15)# Field nos va a permitir poner restricciones de arngos  de valores para validacion
    overview: str = Field(min_length=15, max_length=50) # en field con str se utiliza min_length=5, max_length=15
    year: int = Field(le=2022) ## en el caso de enteros y float  field requiere ge y le  como minimos y maximos 
    rating:float = Field(ge=1, le=10)
    category:str = Field(min_length=5, max_length=15)



    class Config: ##nos funciona para crear un esquema de ejemplo como guia para usuarios. Tiene que ir debajo de la esquema creada con BaseModel 
            json_schema_extra = {
                "example": {
                    "id": 1,
                    "title": "Mi película",
                    "overview": "Descripción de la película",
                    "year": 2022,
                    "rating": 9.8,
                    "category" : "Acción"
                }
            }