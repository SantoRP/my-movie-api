
from fastapi.security import HTTPBearer 
from utils.jwt_manager import create_token, validate_token 
from fastapi import FastAPI, Body, Path, Query,HTTPException, Depends, Request

class JWTBearer(HTTPBearer): ##para la autenticacion
    async def __call__(self, request: Request):
        auth = await super().__call__(request) ## .__call__ its purpose is to create objects that you can call as you would call a regular function. 
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales son invalidas")
