#creacio del middleware para manejo de errores

from __future__ import annotations
from starlette.middleware.base import BaseHTTPMiddleware
from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse


class ErrorHandler(BaseHTTPMiddleware):
    def __init__(self, app: FastAPI) -> None: ##le idicamos que requiere de un aplicacion
        super().__init__(app)## The super() function is a useful tool for accessing and reusing the attributes and methods of a parent class

##etsa funcion ba detectar si existe un error en nuestra aplicacion
    async def dispatch(self, request: Request, call_next) -> Response | JSONResponse: #async make  One step follows another until it’s done. You’re doing all the work yourself.
        try:                                               ##esto de arriba  quiere decir que puede devolver un repsonse o un Json response
            return await call_next(request) #await porque es una funcion async 
        except Exception as e:
            return JSONResponse(status_code=500, content={'error': str(e)})
