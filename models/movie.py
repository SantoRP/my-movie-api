from config.database import Base
from sqlalchemy import Column, Integer, String, Float

## de etsta manera creamos con sqlite la  tabla movies en la basde MOvies 
class Movie(Base): ### Base viene de el .py para la construccion de la database 

    __tablename__ = "movies" 

    id = Column(Integer, primary_key = True)
    title = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String)