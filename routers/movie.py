from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie ##separamos los esquemas del router y los mandasmoa a una archivo de la carpeta schemas/movie.py

movie_router = APIRouter()

##3 aqui copiamos todos los app.get en general todo los de movie que esta en main.py 
##depues susitituimos el app por movie_router 


@movie_router.get('/movies', tags=['movies'], response_model=list[Movie], status_code= 200) #dependencies=[Depends(JWTBearer())]) ### con paqueteria list podemos decir que nos regrese una lista con los diccionarios esto puede ir aqui o en la funcion como en el ejemplo siquiente 
def get_movies()->list[Movie] : ##aqui tambien podemos decir que nos regrese una lusta podemos ponerlo arriba o aca 
    db= Session() #inciamos la sesion 
    result= MovieService(db).get_movies() #get_moivies de aqui es un metodo de la clase MOvieService 
    return JSONResponse(status_code= 200, content=jsonable_encoder(result)) ##para retornarlo como repuesta en formato json con JSONResponse litearlmente es retornarlo normal pero agregando ese JSONResponse y dentro el contenido de lo que quewremos retornar 
        ## status_code es para saber el estado de la accion si fue exitosa o no etc.

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)# el parametro  '/movies/{id}' nos va mostrar eb la app que  se neceita el valor de de ID id  esto es conocido como parametrode ruta
def get_movie(id: int = Path(ge=1,le=2000))-> Movie: ##Path para validar parametros de ruta y no pasarnos de igual manera de los rangos, en el caso de validar maismos y minosm de strings se utiliza Query(max min_length)
    db=Session()
    result= MovieService(db).get_movie(id)#get_movie es la funcion que se encuentra en nuestra clase MovieService
    if not result:
         return JSONResponse(status_code=404, content={'message':'ID no encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
     ###este codigo es para que suelte error 404 sino encuentra ese id  

@movie_router.get('/movies/', tags=['movies'], response_model=list[Movie]) ###cuando no se esta especificando un parametro en la url como en el caso {id}  es porque lo va reconocer como un parametro de  query, y solo hay que indicar lo que se va a buscar en la funcion (eb este caso "category") 
def get_movies_by_category(category: str= Query(min_length=5 ,max_length=15))->list[Movie]:

    db=Session() #para entrar a una sesion 
    result= MovieService(db).get_category(category)#recordar que MovieMOdel es donde es el modulo donde se genera nuestra base de datos   
    if not result:
         return JSONResponse(status_code=404, content={'message':'Categoria no encontrada'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) ##el resultado tiene que tener jsonable_encoder() para poder ser leido 


@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code= 201)  
def create_movie(movie: Movie)-> dict: ##en estas como devuelve diccionario de coloca ->dict y response_model=dict
    db= Session() ##para poder conectarse a a base de datos database.sqlite y poder ingresas los registros 
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code= 201, content={"message": "Se ha registrado la película"})  ###en este caso ya np retornamos el el diccionario con los nuevos datos agrgados sino una repsuesta de ue se logro registrar
                       ##code 201 por que estamos realizando un registro

###put es para actualizar datos en este caso en el diccionario de movies 
@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict,status_code= 200)
def update_movie(id: int, movie: Movie)-> dict:
    db= Session()
    result=MovieService(db).get_movie(id)
    if not result:
         return JSONResponse(status_code=404, content={'message':'ID no encontrado'})
    MovieService(db).update_movie(id,movie)
    return JSONResponse(status_code= 200, content={"message": "Se ha modificado la película"}) ## los mismo sucede con este retorno 

####para eliminar movies usamos delet
@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code= 200)
def delete_movie(id: int) -> dict:
    db= Session()
    result=MovieService(db).get_movie(id)
    if not result:
         return JSONResponse(status_code=404, content={'message':'ID no encontrado'})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code= 200, content={"message": "se ha eliminado la pelicula"}) ## los mismo sucede con este retorno 


