from fastapi import FastAPI, Body, Path, Query,HTTPException, Depends, Request
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field 
from typing import Optional, List
from utils.jwt_manager import create_token, validate_token 
from fastapi.security import HTTPBearer 
from config.database import Session ,engine,Base ##modeulo creado en la corpeta config 
from models.movie import Movie as MovieModel ## importamos la clase qe creamos em models/movie.py
from fastapi.encoders import jsonable_encoder ##para poder retornar el get_movie 
from middlewares.error_handler import ErrorHandler
from middlewares.jwt_bearer import JWTBearer
from routers.movie import movie_router 
from routers.login import login_router 


app = FastAPI()
app.title = "Mi aplicación con  FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)### aqui ya activamose l ErrorHandler para que se ejecute cuando haya un error en la aplicacion
app.include_router(movie_router) ##el router literalmente nos redirecciona nuetrsas funciones de app para tenermas orden todas se encuentran en routers/movie
app.include_router(login_router)

Base.metadata.create_all(bind=engine) ###hace referencia al metodo para vrear las tablas creadas con sqlite




movies = [
    {
		"id": 1,
		"title": "Avatar",
		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		"year": "2009",
		"rating": 7.8,
		"category": "Acción"
	},
    {
		"id": 2,
		"title": "Avatar",
		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		"year": "2009",
		"rating": 7.8,
		"category": "Acción"
	}
]

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')
