## aqui lo que hacemose es la configuracion para la creacion de nuestra bade de datos
##cuando corramos uvicorn se va a crer el archivo database.sqlite"
import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name= "../database.sqlite" ##ponerle nombre a la base de datos y su ubicacion
base_dir= os.path.dirname(os.path.realpath(__file__)) ##establecer el path para leer el archivo database.py

database_url = f"sqlite:///{os.path.join(base_dir,sqlite_file_name)}" ##para unir las urls o paths de la  base_diry el sqlite_file_name 

engine = create_engine(database_url, echo=True)# The create_engine() takes in the connection URL and returns a sqlalchemy enginer that  interpret the DBAPI’s module functions as well as the behavior of the database.

Session = sessionmaker(bind= engine) ###establishes all conversations with the database

Base = declarative_base() #The declarative_base() base class contains a MetaData object where newly defined Table objects are collected.